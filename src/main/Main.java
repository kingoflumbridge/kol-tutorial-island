package main;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.WebWalker;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

import main.misc.*;
import main.one.*;
import main.two.*;
import main.three.*;
import main.four.*;
import main.five.*;
import main.six.*;
import main.seven.*;
import main.eight.*;
import main.nine.*;
import main.ten.*;


@ScriptMeta(developer = "KingOfLumbridge", name = "[KOL] Tutorial Island", desc = "Completes tutorial island ~ 9 mins", category = ScriptCategory.QUESTING)
public class Main extends TaskScript {

    public static void sleepyTime() {
        Time.sleep(400, 600);
    }
    public static int taskSleepyTime() { return Random.nextInt(600, 1000); }

    final Task[] TASKS = {
            // misc
            new ContinueChat(),
            // one
            new EnterName(), new LookUpName(), new SetName(), new CharacterDesign(), new TalkToGielinorGuide(), new ExperienceWithOSRS(), new NavigateOptions(),
            // two
            new GoToFish(), new TalkToSurvivalGuide(), new CatchFish(), new NavigateExperience(),
            // three
            new CookShrimp(), new ExitSurvivalTraining(), new CutTree(), new LightLogs(),
            // four
            new TalkToKitchenGuide(), new MixDough(), new CookDough(), new ExitKitchen(),
            // five
            new ToggleRun(), new TalkToQuestGuide(), new NavigateQuestJournal(), new EnterCave(),
            // six
            new TalkToMiningGuide(), new MineTin(), new MineCopper(), new Smelt(), new UseAnvil(), new SmithDagger(), new ExitMiningArea(),
            // seven
            new TalkToCombatGuide(), new NavigateEquipment(), new NavigateCombat(), new CheckEquipped(), new EquipDagger(), new EquipSwordAndShield(), new EnterRatPen(), new AttackRat(), new RangeRat(), new ExitCave(),
            // eight
            new OpenBank(), new CloseBank(), new OpenPoll(), new ExitPoll(), new TalkToGuide(), new OpenAccountGuide(), new ExitBank(),
            // nine
            new TalkToPrayerGuide(), new NavigatePrayer(), new NavigateFriends(), new ExitChapel(),
            // ten
            new TalkToWizard(), new NavigateMagic(), new AttackChicken(), new GoToMainland(), new NotIronman(), new Logout()
    };

    @Override
    public void onStart(){
        Log.info("Starting");
        Movement.setDefaultWebWalker(WebWalker.Dax);
        submit(TASKS);
    }

    @Override
    public void onStop() {
        Log.info("Stopping");
    }

}
