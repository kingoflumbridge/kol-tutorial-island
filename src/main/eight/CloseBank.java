package main.eight;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CloseBank extends Task {
    @Override
    public boolean validate() {
        return Bank.isOpen();
    }

    @Override
    public int execute() {
        Log.info("[8] Close bank");
        Bank.close();
        Time.sleepUntil(Bank::isClosed, Random.nextInt(2000, 3000));
        return Main.taskSleepyTime();
    }
}
