package main.eight;

import main.misc.Constants;
import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExitBank extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Continue through the next door.")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Follow the path to the chapel and enter it"))
                && Constants.CHAPEL.distance() > 8;
    }

    @Override
    public int execute() {
        Log.info("[8] Exit bank");
        Movement.walkTo(Constants.CHAPEL);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(2000, 3000));
        return Main.taskSleepyTime();
    }
}
