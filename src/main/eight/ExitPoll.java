package main.eight;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExitPoll extends Task {
    Position ACCOUNT_MANAGEMENT_ROOM = new Position(3125, 3124);

    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("When you're ready, move on through the door indicated");
    }

    @Override
    public int execute() {
        Log.info("[8] Exit poll");
        if(Interfaces.getComponent(310, 2, 11) != null){
            Interfaces.getComponent(310, 2, 11).interact(ActionOpcodes.INTERFACE_ACTION);
            Main.sleepyTime();
        }
        Movement.walkTo(ACCOUNT_MANAGEMENT_ROOM);
        return Main.taskSleepyTime();
    }
}
