package main.eight;

import main.misc.Constants;
import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class OpenBank extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("click on the indicated booth");
    }

    @Override
    public int execute() {
        Log.info("[8] Open bank");
        Main.sleepyTime();
        if (Constants.BANK_TILE.distance() > 6) {
            Movement.walkTo(Constants.BANK_TILE);
            Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(2000, 3000));
            return Main.taskSleepyTime();
        }
        SceneObject bankBooth = SceneObjects.getNearest("Bank booth");
        bankBooth.interact("Use");
        Time.sleepUntil(Bank::isOpen, Random.nextInt(2000, 3000));
        return Main.taskSleepyTime();
    }
}
