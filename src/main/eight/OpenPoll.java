package main.eight;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class OpenPoll extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("click on the indicated poll booth");
    }

    @Override
    public int execute() {
        Log.info("[8] Open poll");
        SceneObjects.getNearest("Poll booth").interact("Use");
        Time.sleepUntil(Dialog::canContinue, Random.nextInt(2000, 3000));
        return Main.taskSleepyTime();
    }
}
