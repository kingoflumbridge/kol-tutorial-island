package main.eight;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("The guide here will tell you all about your account")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Talk to the Account Guide"))
                && Interfaces.getComponent(263, 1, 0).isVisible();
    }

    @Override
    public int execute() {
        Log.info("[8] Talk to guide");
        Npcs.getNearest("Account Guide").interact("Talk-to");
        return Main.taskSleepyTime();
    }
}
