package main.five;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class EnterCave extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("It's time to enter some caves");
    }

    @Override
    public int execute() {
        Log.info("[5] Enter cave");
        SceneObjects.getNearest("Ladder").interact("Climb-down");
        Time.sleepUntil(()->!Interfaces.getComponent(263, 1, 0).getText().contains("It's time to enter some caves"), Random.nextInt(5000, 10000));
        return Main.taskSleepyTime();
    }
}
