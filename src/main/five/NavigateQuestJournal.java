package main.five;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigateQuestJournal extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on the flashing icon to the left of your inventory");
    }

    @Override
    public int execute() {
        Log.info("[5] Navigate quest journal");
        Tabs.open(Tab.QUEST_LIST);
        return Main.taskSleepyTime();
    }
}
