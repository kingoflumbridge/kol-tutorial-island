package main.five;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToQuestGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("It's time to learn about quests")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Talk to the quest guide"));
    }

    @Override
    public int execute() {
        Log.info("[5] Talk to quest guide");
        Npc questGuide = Npcs.getNearest("Quest Guide");
        if(questGuide != null){
            questGuide.interact("Talk-to");
            Time.sleepUntil(()-> Dialog.canContinue(), Random.nextInt(2000, 5000));
        }
        return Main.taskSleepyTime();
    }
}
