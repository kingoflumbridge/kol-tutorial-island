package main.five;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ToggleRun extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Running is faster");
    }

    @Override
    public int execute() {
        Log.info("[5] Toggle run");
        if (Movement.isRunEnabled()) {
            Movement.toggleRun(false);
        } else {
            Movement.toggleRun(true);
        }
        return Main.taskSleepyTime();
    }
}
