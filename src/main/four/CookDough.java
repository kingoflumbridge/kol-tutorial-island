package main.four;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CookDough extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Now you have made the dough");
    }

    @Override
    public int execute() {
        Log.info("[4] Cook dough");
        if(!Inventory.isItemSelected()){
            Inventory.getFirst("Bread dough").interact("Use");
            Main.sleepyTime();
        }
        SceneObjects.getNearest("Range").interact("Use");
        Time.sleepUntil(()->Inventory.contains("Bread"), Random.nextInt(1000, 5000));
        return Main.taskSleepyTime();
    }
}
