package main.four;

import main.Main;
import main.misc.Constants;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExitKitchen extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("You've baked your first loaf of bread") ||
                    Interfaces.getComponent(263, 1, 0).getText().contains("follow the path to the next guide"));
    }

    @Override
    public int execute() {
        Log.info("[4] Exit kitchen");
        Movement.walkTo(Constants.QUEST_HOUSE);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(3000, 5000));
        return Main.taskSleepyTime();
    }
}
