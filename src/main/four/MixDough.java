package main.four;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class MixDough extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("To make dough you must mix flour with water");
    }

    @Override
    public int execute() {
        Log.info("[4] Mix dough");
        if(!Inventory.isItemSelected()){
            Inventory.getFirst("Pot of flour").interact("Use");
            Main.sleepyTime();
        }
        Inventory.getFirst("Bucket of water").interact("Use");
        return Main.taskSleepyTime();
    }
}
