package main.four;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToKitchenGuide extends Task {
    @Override
        public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Talk to the chef indicated");
    }

    @Override
    public int execute() {
        Log.info("[4] Talk to kitchen guide");
        Npc chef = Npcs.getNearest("Master Chef");
        if(chef != null){
            chef.interact("Talk-to");
        }
        return Main.taskSleepyTime();
    }
}
