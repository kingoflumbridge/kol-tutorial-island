package main.misc;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class Constants {
    public static Position WIZARD_HOUSE = new Position(3140, 3087);
    public static Position QUEST_HOUSE = new Position(3086, 3125);
    public static Position RAT_PEN = new Position(3110, 9518);
    public static Position BANK_TILE = new Position(3122, 3123);
    public static Position CHAPEL = new Position(3126, 3106);
    public static Area LUMBRIDGE = Area.rectangular(3232, 3214, 3235, 3235);
}
