package main.misc;

import main.Main;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;


public class ContinueChat extends Task {
    @Override
    public boolean validate() {
        return Dialog.canContinue();
    }

    @Override
    public int execute() {
        Log.info("[M] Continue chat");
        Main.sleepyTime();
        Dialog.processContinue();
        return Random.mid(400, 600);
    }
}
