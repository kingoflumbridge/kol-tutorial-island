package main.nine;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static main.misc.Constants.WIZARD_HOUSE;

public class ExitChapel extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("You're almost finished on tutorial island. Pass through the door")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Follow the path to the wizard's house"))
                && WIZARD_HOUSE.distance() > 7;
    }

    @Override
    public int execute() {
        Log.info("[9] Exit chapel");
        Movement.walkTo(WIZARD_HOUSE);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(2000, 3000));
        return Main.taskSleepyTime();
    }
}
