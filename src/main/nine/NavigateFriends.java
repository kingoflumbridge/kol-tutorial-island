package main.nine;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigateFriends extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on the flashing face icon");
    }

    @Override
    public int execute() {
        Log.info("[9] Navigate friends");
        Tabs.open(Tab.FRIENDS_LIST);
        return Main.taskSleepyTime();
    }
}
