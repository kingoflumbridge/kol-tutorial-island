package main.nine;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigatePrayer extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on the flashing icon to open the Prayer menu");
    }

    @Override
    public int execute() {
        Log.info("[9] Navigate prayer");
        Tabs.open(Tab.PRAYER);
        return Main.taskSleepyTime();
    }
}
