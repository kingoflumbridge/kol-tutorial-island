package main.nine;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static main.misc.Constants.CHAPEL;

public class TalkToPrayerGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                &&( Interfaces.getComponent(263, 1, 0).getText().contains("Once inside talk to the monk")
                ||  Interfaces.getComponent(263, 1, 0).getText().contains("Speak with Brother Brace")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Talk with Brother Brace"))
                && CHAPEL.distance() <= 8;
    }

    @Override
    public int execute() {
        Log.info("[9] Talk to prayer guide");
        Npc monk = Npcs.getNearest("Brother Brace");
        monk.interact("Talk-to");
        return Main.taskSleepyTime();
    }
}
