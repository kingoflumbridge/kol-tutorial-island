package main.one;

import main.Main;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CharacterDesign extends Task {
    int[] designComponents = {106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 121, 122, 123, 124, 125, 127, 129, 130, 131, 137};
    @Override
    public boolean validate() {
        return  (Interfaces.getComponent(162, 45) != null
                && Interfaces.getComponent(162, 45).isVisible())
                || Interfaces.getComponent(269, 99) != null;
    }

    @Override
    public int execute() {
        Log.info("[1] Character Design");
        Main.sleepyTime();
        if(Interfaces.getComponent(269, 99) != null) {
            int customizationsCount = Random.nextInt(0, 20);
            for (int i = 0; i < customizationsCount; i++) {
                int designComponent = designComponents[Random.nextInt(0, designComponents.length)];
                Interfaces.getComponent(269, designComponent).interact(ActionOpcodes.BUTTON_INPUT);
                Main.sleepyTime();
            }
            Interfaces.getComponent(269, 99).interact(ActionOpcodes.BUTTON_INPUT);
        } else if(Interfaces.getComponent(162, 45) != null) {
            Game.getClient().fireScriptEvent(299, 1, 1);
        }
        return Main.taskSleepyTime();
    }
}
