package main.one;

import main.Main;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class EnterName extends Task {
    String[] words = {"pottery", "cold", "hot", "smoke", "fire", "wowzers", "white", "blue", "pink", "king", "curtain", "book"};
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(162, 44) != null
                && Interfaces.getComponent(162, 45) != null
                && Interfaces.getComponent(162, 44).getText().contains("Please pick a unique display name")
                && Interfaces.getComponent(162, 44).isVisible()
                && Interfaces.getComponent(162, 45).getText().length() <= 1;
    }

    @Override
    public int execute() {
        Log.info("[1] Enter Name");
        String word1 = words[Random.nextInt(0, words.length)];
        String word2 = words[Random.nextInt(0, words.length)];
        String username = word1 + word2 + Random.nextInt(100, 10000);
        Keyboard.sendText(username);
        Main.sleepyTime();
        Keyboard.pressEnter();
        return Main.taskSleepyTime();
    }
}
