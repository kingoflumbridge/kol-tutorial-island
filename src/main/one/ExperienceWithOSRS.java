package main.one;

import main.Main;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExperienceWithOSRS extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(219, 1, 0) != null
                && Interfaces.getComponent(219, 1, 0).isVisible()
                && Interfaces.getComponent(219, 1, 0).getText().contains("What's your experience");
    }

    @Override
    public int execute() {
        Log.info("[1] Experience With OSRS");
        Dialog.process(1);
        return Main.taskSleepyTime();
    }
}
