package main.one;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class LookUpName extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Before you get started, you'll need a unique display name")
                && !Interfaces.getComponent(558, 12).getText().contains("Great! This display name is");
    }

    @Override
    public int execute() {
        Log.info("[1] Look up name");
        Interfaces.getComponent(558, 17).interact(ActionOpcodes.INTERFACE_ACTION);
        return Main.taskSleepyTime();
    }
}
