package main.one;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.awt.*;

public class NavigateOptions extends Task {

    private static final Rectangle RESIZABLE_BOUNDS = new Rectangle(729, 336, 6, 6);
    private static final Rectangle FIXED_BOUNDS = new Rectangle(706, 373, 6, 6);

    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Please click on the flashing spanner icon");
    }

    @Override
    public int execute() {
        Log.info("[1] Navigate options");
        if (Tabs.getOpen().equals(Tab.OPTIONS)) {
            Tabs.open(Tab.LOGOUT);
            return Main.taskSleepyTime();
        }
        Tabs.open(Tab.OPTIONS);
        Main.sleepyTime();
        if (Interfaces.getComponent(261, 34, 8).getBounds().equals(RESIZABLE_BOUNDS)) {
            Interfaces.getComponent(261, 33).interact(ActionOpcodes.INTERFACE_ACTION);//Set screen to fixed
        }
        return Main.taskSleepyTime();
    }
}
