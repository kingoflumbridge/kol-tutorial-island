package main.one;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class SetName extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(558, 12) != null
                && Interfaces.getComponent(558, 12).getText().contains("Great! This display name is");
    }

    @Override
    public int execute() {
        Log.info("[1] Set name");
        Interfaces.getComponent(558, 18).interact(ActionOpcodes.INTERFACE_ACTION);
        return Main.taskSleepyTime();
    }
}
