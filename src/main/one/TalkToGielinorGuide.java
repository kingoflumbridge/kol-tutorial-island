package main.one;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToGielinorGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("click on the Gielinor Guide")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Talk to the Gielinor Guide to continue"))
                && Interfaces.getComponent(263, 1, 0).isVisible();
    }

    @Override
    public int execute() {
        Log.info("[1] Talk to Gielinor guide");
        Npcs.getNearest("Gielinor Guide").interact("Talk-to");
        return Main.taskSleepyTime();
    }
}
