package main.seven;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class AttackRat extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("It's time to slay some rats")
                ||  Interfaces.getComponent(263, 1, 0).getText().contains("bar shows how much health"));

    }

    @Override
    public int execute() {
        Log.info("[7] Attack rat");
        if(Players.getLocal().getTarget() == null) {
            final Npc rat = Npcs.getNearest(n -> n.getName().equals("Giant rat") && n.getTarget() == null);
            rat.interact("Attack");
            Time.sleepUntil(()->Players.getLocal().getTarget() == null, Random.mid(10000, 15000));
        }

        return Main.taskSleepyTime();
    }
}