package main.seven;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CheckEquipped extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(387, 1) != null
                && Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("This is your worn inventory");
    }

    @Override
    public int execute() {
        Log.info("[7] Check equipped");
        Interfaces.getComponent(387, 1).interact(ActionOpcodes.INTERFACE_ACTION);
        return Main.taskSleepyTime();
    }
}
