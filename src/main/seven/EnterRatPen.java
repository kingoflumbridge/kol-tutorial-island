package main.seven;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static main.misc.Constants.RAT_PEN;

public class EnterRatPen extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("monsters are weak to specific attack styles");
    }


    @Override
    public int execute() {
        Log.info("[7] Enter rat pen");
        Movement.walkTo(RAT_PEN);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(2000, 5000));
        return Main.taskSleepyTime();
    }
}