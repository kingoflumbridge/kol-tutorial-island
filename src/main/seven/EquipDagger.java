package main.seven;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class EquipDagger extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click your dagger to equip it");
    }

    @Override
    public int execute() {
        Log.info("[7] Equip dagger");
        Inventory.getFirst("Bronze dagger").interact("Wield");
        return Main.taskSleepyTime();
    }
}
