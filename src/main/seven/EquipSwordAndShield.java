package main.seven;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class EquipSwordAndShield extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("swapping your dagger for the sword and shield");
    }

    @Override
    public int execute() {
        Log.info("[7] Equip sword and shield");
        if (Inventory.contains("Bronze sword")) {
            Inventory.getFirst("Bronze sword").interact("Wield");
            Main.sleepyTime();
        }
        if (Inventory.contains("Wooden shield")) {
            Inventory.getFirst("Wooden shield").interact("Wield");
            Main.sleepyTime();
        }
        return Main.taskSleepyTime();
    }
}
