package main.seven;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExitCave extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("You have completed the tasks here");
    }

    @Override
    public int execute() {
        Log.info("[7] Exit cave");
        SceneObjects.getNearest("Ladder").interact("Climb-up");
        Time.sleepUntil(()->!Players.getLocal().isAnimating(), Random.mid(5000, 10000));
        return Main.taskSleepyTime();
    }
}
