package main.seven;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigateCombat extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on the flashing crossed swords");
    }

    @Override
    public int execute() {
        Log.info("[7] Navigate combat");
        Tabs.open(Tab.COMBAT);
        return Main.taskSleepyTime();
    }
}
