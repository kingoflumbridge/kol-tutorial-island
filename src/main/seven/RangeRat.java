package main.seven;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class RangeRat extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Now you have a bow and some arrows");
    }

    @Override
    public int execute() {
        Log.info("[7] Range rat");
        if(Inventory.contains("Shortbow")){
            Inventory.getFirst("Shortbow").interact("Wield");
            Main.sleepyTime();
        }
        if(Inventory.contains("Bronze arrow")){
            Inventory.getFirst("Bronze arrow").interact("Wield");
            Main.sleepyTime();
        }
        Npcs.getNearest("Giant rat").interact("Attack");
        Time.sleepUntil(()->Players.getLocal().getTarget() == null, Random.nextInt(2000, 3000));
        Main.sleepyTime();

        return Main.taskSleepyTime();
    }
}
