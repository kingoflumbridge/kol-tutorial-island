package main.seven;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToCombatGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Speak to the guide and he will tell you all about it")
                || Interfaces.getComponent(263, 1, 0).getText().contains("to the combat instructor"))
                && !Interfaces.getComponent(263, 1, 0).getText().contains("click on the indicated ladder");
    }

    @Override
    public int execute() {
        Log.info("[7] Talk to combat guide");
        if (Interfaces.getComponent(84, 4) != null) {
            Interfaces.getComponent(84, 4).interact(ActionOpcodes.INTERFACE_ACTION);
            Main.sleepyTime();
        }
        Npc combatGuide = Npcs.getNearest("Combat Instructor");
        if (!combatGuide.isPositionInteractable()) {
            Movement.walkTo(combatGuide.getPosition());
            Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(1500, 15000));
            return Main.taskSleepyTime();
        }
        if (combatGuide != null) {
            combatGuide.interact("Talk-to");
            Time.sleepUntil(() -> Dialog.canContinue(), Random.nextInt(5000, 15000));
        }
        return Main.taskSleepyTime();
    }
}
