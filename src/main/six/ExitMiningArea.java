package main.six;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class ExitMiningArea extends Task {
    Position OUTSIDE_MINING_AREA = new Position(3095, 9502);

    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Congratulations, you've made your first weapon");
    }

    @Override
    public int execute() {
        Log.info("[6] Exit mining area");
        Movement.walkTo(OUTSIDE_MINING_AREA);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(2000, 5000));
        return Main.taskSleepyTime();
    }
}
