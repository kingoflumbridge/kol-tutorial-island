package main.six;

import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class MineCopper extends Task {
    int COPPER_ROCK = 10079;
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("you just need some copper");
    }

    @Override
    public int execute() {
        Log.info("[6] Mine copper");
        SceneObject rock = SceneObjects.getNearest(COPPER_ROCK);
        if(rock != null){
            rock.interact("Mine");
            Time.sleepUntil(Dialog::canContinue, Random.nextInt(10000, 15000));
        }
        return Main.taskSleepyTime();
    }
}
