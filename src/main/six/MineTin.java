package main.six;

import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class MineTin extends Task {
    int TIN_ROCK = 10080;
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("try mining some tin");
    }

    @Override
    public int execute() {
        Log.info("[6] Mine tin");
        SceneObject rock = SceneObjects.getNearest(TIN_ROCK);
        if(rock != null){
            rock.interact("Mine");
            Time.sleepUntil(Dialog::canContinue, Random.nextInt(10000, 15000));
        }
        return Main.taskSleepyTime();
    }
}
