package main.six;

import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Smelt extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("You can smelt these into a bronze bar");
    }

    @Override
    public int execute() {
        Log.info("[6] Smelt");
        if (!Inventory.isItemSelected()) {
            Inventory.getFirst("Tin ore").interact("Use");
            Main.sleepyTime();
        }
        SceneObject furnace = SceneObjects.getNearest("Furnace");
        if (furnace != null) {
            furnace.interact("Use");
            Time.sleepUntil(Dialog::canContinue, Random.nextInt(10000, 15000));
        }
        return Main.taskSleepyTime();
    }
}
