package main.six;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class SmithDagger extends Task {
//    int SMITHING_COMPONENT = 312;
//    int DAGGER_COMPONENT = 9;

    @Override
    public boolean validate() {
        return Interfaces.getComponent(312, 9) != null;
    }

    @Override
    public int execute() {
        Log.info("[6] Smith dagger");
        Interfaces.getComponent(312, 9).interact(ActionOpcodes.INTERFACE_ACTION);
        Time.sleepUntil(()->!Interfaces.getComponent(263, 1, 0).getText().contains("You can smelt these into a bronze bar"), Random.nextInt(10000, 15000));
        Main.sleepyTime();

        return Main.taskSleepyTime();
    }
}
