package main.six;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToMiningGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Next let's get you a weapon")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Speak to the mining instructor"))
                && !Interfaces.getComponent(263, 1, 0).getText().contains("Speak to the mining instructor for a recap");
    }

    @Override
    public int execute() {
        Log.info("[6] Talk to mining guide");
        Npc miningGuide = Npcs.getNearest("Mining Instructor");
        if(miningGuide != null){
            miningGuide.interact("Talk-to");
            Time.sleepUntil(()-> Dialog.canContinue(), Random.nextInt(5000, 10000));
        }
        return Main.taskSleepyTime();
    }
}
