package main.ten;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class AttackChicken extends Task {
    @Override
    public boolean validate() {
        return Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on this spell to select it");
    }

    @Override
    public int execute() {
        Log.info("[10] Attack chicken");
        Npc chicken = Npcs.getNearest("Chicken");
        Magic.cast(Spell.Modern.WIND_STRIKE, chicken);
        return Main.taskSleepyTime();
    }
}
