package main.ten;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class GoToMainland extends Task {
    @Override
    public boolean validate() {
        return Interfaces.getComponent(219, 1, 0) != null
                && Interfaces.getComponent(219, 1, 0).getText().contains("go to the mainland");
    }

    @Override
    public int execute() {
        Log.info("[10] Go to mainland");
        Interfaces.getComponent(219, 1, 1).interact(ActionOpcodes.BUTTON_DIALOG);
        return Main.taskSleepyTime();
    }
}
