package main.ten;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;
import static main.misc.Constants.LUMBRIDGE;

public class Logout extends Task {
    @Override
    public boolean validate() {
        return LUMBRIDGE.contains(Players.getLocal());
    }

    @Override
    public int execute() {
        Log.info("[10] Logout");
        Game.logout();
        return -1;
    }
}
