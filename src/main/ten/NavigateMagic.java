package main.ten;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigateMagic extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Open up the magic interface");
    }

    @Override
    public int execute() {
        Log.info("[10] Navigate magic");
        Tabs.open(Tab.MAGIC);
        return Main.taskSleepyTime();
    }
}
