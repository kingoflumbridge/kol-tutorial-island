package main.ten;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.menu.ActionOpcodes;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NotIronman extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(219, 1, 3) != null
                && Interfaces.getComponent(219, 1, 3).getText().contains("No, I'm not planning to do that");
    }

    @Override
    public int execute() {
        Log.info("[10] Not iron man");
        Interfaces.getComponent(219, 1, 3).interact(ActionOpcodes.BUTTON_DIALOG);
        return Main.taskSleepyTime();
    }
}
