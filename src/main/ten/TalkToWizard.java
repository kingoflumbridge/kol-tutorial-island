package main.ten;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static main.misc.Constants.WIZARD_HOUSE;

public class TalkToWizard extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Follow the path to the wizard's house")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Your spells are listed here. Talk to the instructor")
                || Interfaces.getComponent(263, 1, 0).getText().contains("spells can be found here")
                || Interfaces.getComponent(263, 1, 0).getText().contains("speak with the magic instructor"))
                && Interfaces.getComponent(263, 1, 0).isVisible()
                && WIZARD_HOUSE.distance() <= 7;
    }

    @Override
    public int execute() {
        Log.info("[10] Talk to wizard");
        Npc monk = Npcs.getNearest("Magic Instructor");
        monk.interact("Talk-to");
        return Main.taskSleepyTime();
    }
}
