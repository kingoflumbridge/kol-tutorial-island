package main.three;

import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CookShrimp extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Now it's time to get cooking")
                && Inventory.contains("Raw shrimps")
                && !Inventory.contains("Shrimps")
                && SceneObjects.getNearest("Fire") != null
                && Players.getLocal().getAnimation() == -1;
    }

    @Override
    public int execute() {
        Log.info("[3] Cook shrimp");
        if(!Inventory.isItemSelected()){
            if(Inventory.getFirst("Raw shrimps").interact("Use")){
                Main.sleepyTime();
            }
        }
        SceneObject fire = SceneObjects.getNearest("Fire");
        if(fire != null){
            if(fire.interact("Use")){
                Time.sleepUntil(()->Inventory.contains("Shrimps"), Random.mid(1000, 5000));
            }
        }
        return Main.taskSleepyTime();
    }
}
