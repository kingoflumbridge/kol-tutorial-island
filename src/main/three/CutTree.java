package main.three;

import main.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CutTree extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Give it a go by clicking on one of the trees in the area")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Now it's time to get cooking")
                && SceneObjects.getNearest("Fire") == null
                && !Inventory.contains("Logs"))
                && Players.getLocal().getAnimation() == -1;
    }

    @Override
    public int execute() {
        Log.info("[3] Cut tree");
        SceneObject tree = SceneObjects.getNearest("Tree");
        if(tree != null){
            tree.interact("Chop down");
            Time.sleep(Random.nextInt(10000, 15000), Random.nextInt(15000, 20000));
        }
        return Main.taskSleepyTime();
    }
}
