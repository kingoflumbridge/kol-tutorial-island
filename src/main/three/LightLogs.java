package main.three;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class LightLogs extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("it's time to light a fire")
                || (Interfaces.getComponent(263, 1, 0).getText().contains("Now it's time to get cooking")
                && SceneObjects.getNearest("Fire") == null
                && Inventory.contains("Logs"))
                && Players.getLocal().getAnimation() == -1);
    }

    @Override
    public int execute() {
        Log.info("[3] Light logs");
        Position ourPosition = Players.getLocal().getPosition();
        if (SceneObjects.newQuery().names("Fire").on(ourPosition).results().size() > 0) {
            Position newPosition = ourPosition.randomize(1);
            while (!newPosition.isPositionWalkable()) {
                newPosition = ourPosition.randomize(1);
            }
            Movement.walkTo(newPosition);
            return Main.taskSleepyTime();
        }
        if (!Inventory.isItemSelected()) {
            if (Inventory.getFirst("Tinderbox").interact("Use")) {
                Main.sleepyTime();
            }
        } else {
            if (Inventory.getFirst("Logs").interact("Use")) {
                Time.sleep(Random.nextInt(10000, 15000), Random.nextInt(15000, 25000));
            }
        }
        return Main.taskSleepyTime();
    }
}
