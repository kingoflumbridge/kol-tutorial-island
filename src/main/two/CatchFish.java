package main.two;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class CatchFish extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("click on the flashing backpack icon")
         || Interfaces.getComponent(263, 1, 0).getText().contains("use it to catch some shrimp"));
    }

    @Override
    public int execute() {
        Log.info("[2] Catch fish");
        if (!Tabs.getOpen().equals(Tab.INVENTORY)) {
            Tabs.open(Tab.INVENTORY);
            Main.sleepyTime();
        }
        Npc fishingSpot = Npcs.getNearest("Fishing spot");
        if (fishingSpot != null) {
            fishingSpot.interact("Net");
        }
        return Main.taskSleepyTime();
    }
}
