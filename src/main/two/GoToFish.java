package main.two;

import main.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.Path;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class GoToFish extends Task {
    private Position FISHING_SPOT = new Position(3102, 3096);
    Path path = null;

    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("time to meet your first instructor")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Follow the path to find the next instructor"))
                && FISHING_SPOT.distance() > 8;
    }

    @Override
    public int execute() {
        Log.info("[2] Go to fish");
        Movement.walkTo(FISHING_SPOT);
        Time.sleepUntil(() -> Movement.getDestinationDistance() <= 7, Random.nextInt(1000, 5000));
        return Main.taskSleepyTime();
    }
}
