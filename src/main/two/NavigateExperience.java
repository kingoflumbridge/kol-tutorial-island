package main.two;

import main.Main;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NavigateExperience extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && Interfaces.getComponent(263, 1, 0).getText().contains("Click on the flashing bar");
    }

    @Override
    public int execute() {
        Log.info("[2] Navigate experience");
        Tabs.open(Tab.SKILLS);
        return Main.taskSleepyTime();
    }
}
