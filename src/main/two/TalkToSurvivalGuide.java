package main.two;

import main.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class TalkToSurvivalGuide extends Task {
    @Override
    public boolean validate() {
        return  Interfaces.getComponent(263, 1, 0) != null
                && (Interfaces.getComponent(263, 1, 0).getText().contains("Talk to the survival expert to continue")
                || Interfaces.getComponent(263, 1, 0).getText().contains("Speak to the survival expert to continue"));
    }

    @Override
    public int execute() {
        Log.info("[2] Talk to survival guide");
        Npc survivalExpert = Npcs.getNearest("Survival Expert");
        if (survivalExpert.interact("Talk-to")) {
            return Main.taskSleepyTime();
        }
        return Main.taskSleepyTime();
    }
}
